for f in *; do
    if [ -d ${f} ]; then
      echo $f
      docker pull avtandilko/$f
      docker tag avtandilko/$f avtandilko/$f:v0.0.1
      docker push avtandilko/$f:v0.0.1
    fi
done
